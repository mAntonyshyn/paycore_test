<?php

namespace App;

/**
 * Class Image
 * @package App
 */
class Image extends AbstractBlock
{
    /**
     * @return string
     */
    public function renderComposition(): string
    {
        $this->content = "<img class=\"{$this->getClassName()}\" alt=\"{$this->getTitle()}\">";

        return $this->content;
    }

    public function render(): void
    {
        $this->content = "<img class=\"{$this->getClassName()}\" alt=\"{$this->getTitle()}\">";

        echo $this->content;
    }
}