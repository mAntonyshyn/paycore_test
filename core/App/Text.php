<?php

namespace App;

use Composite\AbstractBlockComposite;

/**
 * Class Text
 * @package App
 */
class Text extends AbstractBlockComposite
{
    /**
     * @return string
     */
    public function renderComposition(): string
    {
        $output = parent::renderComposition();
        $this->content = "<div class=\"{$this->getClassName()}\">{$this->getTitle()}$output</div>";

        return $this->content;
    }

    public function render(): void
    {
        $this->content = "<div class=\"{$this->getClassName()}\">{$this->getTitle()}</div>";

        echo $this->content;
    }

    /**
     * @param array $arr
     */
    public function renderPlaceholder(array $arr): void
    {
        foreach ($arr as $item){
            foreach ($this->fields as $value) {
                if ($value instanceof $item){
                    echo $value->content;
                }
            }
        }
    }
}