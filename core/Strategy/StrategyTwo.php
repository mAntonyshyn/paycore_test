<?php

namespace Strategy;

use App\AbstractBlock;

class StrategyTwo implements StrategyInterface
{
    /**
     * @var array
     */
    protected $fields = array();

    /**
     * StrategyTwo constructor.
     * @param $fields
     */
    public function __construct($fields)
    {
        $this->fields = $fields;
    }

    /**
     * @param AbstractBlock $block
     * @return mixed|string
     */
    final public function plugin(AbstractBlock $block): string
    {
        $prefix = $this->prefix($block);
        $postfix = $this->postfix($block);

        return  $prefix.$postfix;
    }

    /**
     * @param AbstractBlock $block
     * @return mixed|string
     */
    public function prefix(AbstractBlock $block): string
    {
        $str = $this->renderNotPlaceholder($this->fields, $block);

        return "$str{$block->renderComposition()}";
    }

    /**
     * @param AbstractBlock $block
     * @return mixed|string
     */
    public function postfix(AbstractBlock $block): string
    {
        $str = $this->renderNotPlaceholder($this->fields, $block);

        return $str;
    }

    /**
     * @param array $arr
     * @param AbstractBlock $block
     * @return string
     */
    public function renderNotPlaceholder(array $arr, AbstractBlock $block): string
    {
        $str = "";
        $arr1 = $block->getChildren();

        foreach ($arr as $key1 => $item){
            foreach ($arr1 as $key2 => $value) {
                if ($value instanceof $item){
                    unset($arr1[$key2]);
                }
                continue;
            }
        }

        foreach ($arr1 as $item) {
            $str .= $item->getContent();
        }

        return $str;
    }
}