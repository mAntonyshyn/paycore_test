<?php
namespace Strategy;

use App\AbstractBlock;

/**
 * Interface StrategyInterface
 * @package Strategy
 */
interface StrategyInterface
{
    /**
     * @param AbstractBlock $block
     * @return string
     */
    public function plugin(AbstractBlock $block): string;

    /**
     * @param AbstractBlock $block
     * @return string
     */
    public function prefix(AbstractBlock $block): string;

    /**
     * @param AbstractBlock $block
     * @return string
     */
    public function postfix(AbstractBlock $block): string;
}