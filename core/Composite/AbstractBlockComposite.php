<?php

namespace Composite;

use App\AbstractBlock;

/**
 * Class AbstractBlockComposite
 * @package Composite
 */
abstract class AbstractBlockComposite extends AbstractBlock
{
    /**
     * @var array
     */
    protected $fields = array();

    /**
     * @param AbstractBlock $abstractBlock
     */
    public function add(AbstractBlock $abstractBlock): void
    {
        $this->fields[] = $abstractBlock;
    }

    /**
     * @return AbstractBlockComposite
     */
    public function getFamily(): AbstractBlockComposite
    {
        return $this;
    }

    /**
     * @return string
     */
    public function renderComposition(): string
    {
        $output = "";

        foreach ($this->fields as $name => $field) {
            $output .= $field->renderComposition();
        }

        return $output;
    }

    /**
     * @param array $arr
     */
    abstract public function renderPlaceholder(array $arr): void;
}