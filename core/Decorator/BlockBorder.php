<?php

namespace Decorator;

/**
 * Class BlockBorder
 * @package Decorator
 */
class BlockBorder extends AbstractBorderDecorator
{
    /**
     * @param \App\AbstractBlock $block
     * @return string
     */
    public function changeBlock($block): string
    {
        $content = $block->getContent();
        $blockBegin = '<style>.'.$block->getClassName().'{border: '.$this->width.'px solid '.$this->color.'; padding: 5px;}</style>';

        return $blockBegin.$content;
    }
}